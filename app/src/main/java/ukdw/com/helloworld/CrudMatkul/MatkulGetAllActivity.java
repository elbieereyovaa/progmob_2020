package ukdw.com.helloworld.CrudMatkul;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.helloworld.Adapter.MatkulCRUDAdapter;
import ukdw.com.helloworld.Model.Matkul;
import ukdw.com.helloworld.Network.GetDataService;
import ukdw.com.helloworld.Network.RetrofitClientInstance;
import ukdw.com.helloworld.R;

public class MatkulGetAllActivity extends AppCompatActivity {
    RecyclerView rvMatkul;
    MatkulCRUDAdapter matkulCRUDAdapter;
    ProgressDialog pd;
    List<Matkul> matkulList; //data akan diisi ke internet
    LinearLayout currentLayout;
    Snackbar mySnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_get_all);

        rvMatkul = (RecyclerView) findViewById(R.id.rvGetMatkulAll);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Bersabar"); //text akan muncul ketika melakukan pemanggilan data
        pd.show(); //lingkaran loading
        Toolbar myToolBar = (Toolbar)findViewById(R.id.toolbarMatkul);
        setSupportActionBar(myToolBar);
        //ganti title bar
        getSupportActionBar().setTitle("Data Mata Kuliah");

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        //memanggil service yang nantinya mengembalikan list mahasiswa
        Call<List<Matkul>> call = service.getMatkul("72180196");

        //dengan menggunakan objek call, akan dilakukan enqueue (diurutkan)
        call.enqueue(new Callback<List<Matkul>>() {
            @Override
            //fungsi yang berjalan ketika berhasil mengolah data
            public void onResponse(Call<List<Matkul>> call, Response<List<Matkul>> response) {
                pd.dismiss(); //menghilangkan progress dialog (lingkaran loading)
                matkulList = response.body(); //mahasiswa list diisi dari hasil respon
                matkulCRUDAdapter = new MatkulCRUDAdapter(matkulList); //lalu diisi ke adapter

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MatkulGetAllActivity.this);
                rvMatkul.setLayoutManager(layoutManager);
                rvMatkul.setAdapter(matkulCRUDAdapter);
            }
            @Override
            //onfailure akan terjadi jika gagal dalam mengolah data
            public void onFailure(Call<List<Matkul>> call, Throwable t) {
                pd.dismiss();
                //dikasih alert kalo terjadi error
                Toast.makeText(MatkulGetAllActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenumatkultambah, menu);
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tambahMatkul:
                Intent intent = new Intent(this, MatkulAddActivity.class);
                startActivity(intent);
                return true;
            case R.id.hapusMatkul:
                Intent intentHps = new Intent(this, HapusMatkulActivity.class);
                startActivity(intentHps);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}