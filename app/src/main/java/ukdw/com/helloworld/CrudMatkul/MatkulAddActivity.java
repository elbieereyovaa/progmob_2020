package ukdw.com.helloworld.CrudMatkul;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.helloworld.Crud.MahasiswaAddActivity;
import ukdw.com.helloworld.Crud.MahasiswaGetAllActivity;
import ukdw.com.helloworld.Model.DefaultResult;
import ukdw.com.helloworld.Network.GetDataService;
import ukdw.com.helloworld.Network.RetrofitClientInstance;
import ukdw.com.helloworld.R;

public class MatkulAddActivity extends AppCompatActivity {
    ProgressDialog pd;
    LinearLayout currentLayout;
    Snackbar mySnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_add);

        EditText edKode = (EditText)findViewById(R.id.editTextKode);
        EditText edNama = (EditText)findViewById(R.id.editTextNamaMatkul);
        EditText edHari = (EditText)findViewById(R.id.editTextHari);
        EditText edSesi = (EditText)findViewById(R.id.editTextSesi);
        EditText edSks = (EditText)findViewById(R.id.editTextSks);
        Button btnSimpan = (Button)findViewById(R.id.btnSimpanMatkul);
        pd = new ProgressDialog(MatkulAddActivity.this);
        Toolbar myToolBar = (Toolbar)findViewById(R.id.toolbarAddMatkul);
        setSupportActionBar(myToolBar);
        //ganti title bar
        getSupportActionBar().setTitle("Tambah Data Matakuliah");

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //membuat progress dialog
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                //memanggil service yang nantinya mengembalikan list mahasiswa
                Call<DefaultResult> call = service.add_matkul(
                        edNama.getText().toString(),
                        "72180196",
                        edKode.getText().toString(),
                        Integer.parseInt(edHari.getText().toString()),
                        Integer.parseInt(edSesi.getText().toString()),
                        Integer.parseInt(edSks.getText().toString())
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MatkulAddActivity.this,"DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulAddActivity.this,"GAGAL", Toast.LENGTH_LONG).show();
                    }
                });
                Intent intent = new Intent(MatkulAddActivity.this, MatkulGetAllActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}