package ukdw.com.helloworld.CrudMatkul;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.helloworld.Crud.HapusMhsActivity;
import ukdw.com.helloworld.Crud.MahasiswaGetAllActivity;
import ukdw.com.helloworld.Model.DefaultResult;
import ukdw.com.helloworld.Network.GetDataService;
import ukdw.com.helloworld.Network.RetrofitClientInstance;
import ukdw.com.helloworld.R;

public class HapusMatkulActivity extends AppCompatActivity {
    ProgressDialog pd;
    LinearLayout currentLayout;
    Snackbar mySnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hapus_matkul);

        EditText edKodeHps = (EditText)findViewById(R.id.editTextKode);
        Button btnHpsMatkul = (Button)findViewById(R.id.btnHapusMatkul);
        pd = new ProgressDialog(HapusMatkulActivity.this);
        Toolbar myToolBar = (Toolbar)findViewById(R.id.toolbarHpsMatkul);
        setSupportActionBar(myToolBar);
        //ganti title bar
        getSupportActionBar().setTitle("Hapus Data Matkul");

        btnHpsMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //membuat progress dialog
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                //memanggil service yang nantinya mengembalikan list mahasiswa
                Call<DefaultResult> call = service.delete_matkul(
                        edKodeHps.getText().toString(),
                        "72180196"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(HapusMatkulActivity.this,"DATA BERHASIL DIHAPUS", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(HapusMatkulActivity.this,"GAGAL", Toast.LENGTH_LONG).show();
                    }
                });
                Intent intent = new Intent(HapusMatkulActivity.this, MatkulGetAllActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}