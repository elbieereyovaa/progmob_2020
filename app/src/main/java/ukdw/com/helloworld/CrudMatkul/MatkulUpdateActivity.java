package ukdw.com.helloworld.CrudMatkul;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.helloworld.Model.DefaultResult;
import ukdw.com.helloworld.Network.GetDataService;
import ukdw.com.helloworld.Network.RetrofitClientInstance;
import ukdw.com.helloworld.R;
import ukdw.com.helloworld.crudDosen.DosenGetAllActivity;
import ukdw.com.helloworld.crudDosen.DosenUpdateActivity;

public class MatkulUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;
    Snackbar mySnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_update);
        Toolbar myToolBar = (Toolbar)findViewById(R.id.toolbarUpdateMatkul);
        setSupportActionBar(myToolBar);
        //ganti title bar
        getSupportActionBar().setTitle("Update Data Matakuliah");

        EditText edKodeBaru = (EditText)findViewById(R.id.editTextKode2);
        EditText edNamaBaru = (EditText)findViewById(R.id.editTextNamaMatkul2);
        EditText edHariBaru = (EditText)findViewById(R.id.editTextHari2);
        EditText edSesiBaru = (EditText)findViewById(R.id.editTextSesi2);
        EditText edSksBaru = (EditText)findViewById(R.id.editTextSks2);
        Button btnSimpan = (Button)findViewById(R.id.btnUpdate);
        EditText edKodeCari = (EditText)findViewById(R.id.editTextKodeCari);
        pd = new ProgressDialog(MatkulUpdateActivity.this);

        Intent data = getIntent();
        if(data.getExtras() != null) {
            edKodeCari.setText(data.getStringExtra("kode"));
            edNamaBaru.setText(data.getStringExtra("nama"));
            edKodeBaru.setText(data.getStringExtra("kode"));
            edHariBaru.setText(data.getStringExtra("hari"));
            edSesiBaru.setText(data.getStringExtra("sesi"));
            edSksBaru.setText(data.getStringExtra("sks"));
        }

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.setCancelable(false);
                pd.show();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                //memanggil service yang nantinya mengembalikan list mahasiswa
                Call<DefaultResult> call = service.update_matkul(
                        edNamaBaru.getText().toString(),
                        "72180196",
                        edKodeBaru.getText().toString(),
                        Integer.parseInt(edHariBaru.getText().toString()),
                        Integer.parseInt(edSesiBaru.getText().toString()),
                        Integer.parseInt(edSksBaru.getText().toString()),
                        edKodeCari.getText().toString()
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this, "DATA BERHASIL DIUBAH", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
                    }
                });
                Intent intentUpdate = new Intent(MatkulUpdateActivity.this, MatkulGetAllActivity.class);
                startActivity(intentUpdate);
                finish();
            }
        });
    }
}