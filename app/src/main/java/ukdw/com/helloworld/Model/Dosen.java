package ukdw.com.helloworld.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dosen {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("foto")
    @Expose
    private String imgDsn;

    @SerializedName("nidn")
    @Expose
    private String nidn;

    @SerializedName("nama")
    @Expose
    private String nama;

    @SerializedName("gelar")
    @Expose
    private String gelar;

    private String noTelp;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("alamat")
    @Expose
    private String alamat;

    @SerializedName("nim_progmob")
    @Expose
    private String nim_progmob;

    public Dosen(String nama, String nidn, String noTelp) {
        this.nama = nama;
        this.nidn = nidn;
        this.noTelp = noTelp;
    }

    public Dosen(String id, String imgDsn, String nidn, String nama, String gelar, String noTelp, String email, String alamat, String nim_progmob) {
        this.id = id;
        this.imgDsn = imgDsn;
        this.nidn = nidn;
        this.nama = nama;
        this.gelar = gelar;
        this.noTelp = noTelp;
        this.email = email;
        this.alamat = alamat;
        this.nim_progmob = nim_progmob;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImgDsn() {
        return imgDsn;
    }

    public void setImgDsn(String imgDsn) {
        this.imgDsn = imgDsn;
    }

    public String getNidn() {
        return nidn;
    }

    public void setNidn(String nidn) {
        this.nidn = nidn;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGelar() {
        return gelar;
    }

    public void setGelar(String gelar) {
        this.gelar = gelar;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNim_progmob() {
        return nim_progmob;
    }

    public void setNim_progmob(String nim_progmob) {
        this.nim_progmob = nim_progmob;
    }
}
