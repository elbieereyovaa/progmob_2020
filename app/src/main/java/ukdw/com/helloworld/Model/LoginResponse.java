package ukdw.com.helloworld.Model;

public class LoginResponse {
    private UserData user;

    public LoginResponse(UserData user){
        this.user = user;
    }

    public UserData getUser() {
        return user;
    }

    public void setUser(UserData user) {
        this.user = user;
    }
}
