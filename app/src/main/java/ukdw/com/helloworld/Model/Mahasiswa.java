package ukdw.com.helloworld.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Mahasiswa {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("foto")
    @Expose
    private String imgMhs;

    @SerializedName("nim")
    @Expose
    private String nim;

    @SerializedName("nama")
    @Expose
    private String nama;

    private String noTelp;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("alamat")
    @Expose
    private String alamat;

    @SerializedName("nim_progmob")
    @Expose
    private String nim_progmob;

    public Mahasiswa(String nim, String nama, String noTelp) {
        this.nim = nim;
        this.nama = nama;
        this.email = noTelp;
    }

    public Mahasiswa(String id, String imgMhs, String nim, String nama, String noTelp, String email, String alamat, String nim_progmob) {
        this.id = id;
        this.imgMhs = imgMhs;
        this.nim = nim;
        this.nama = nama;
        this.noTelp = noTelp;
        this.email = email;
        this.alamat = alamat;
        this.nim_progmob = nim_progmob;
    }

    public Mahasiswa(String imgMhs, String nim, String nama, String noTelp, String email, String alamat, String nim_progmob) {
        this.imgMhs = imgMhs;
        this.nim = nim;
        this.nama = nama;
        this.noTelp = noTelp;
        this.email = email;
        this.alamat = alamat;
        this.nim_progmob = nim_progmob;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImgMhs() {
        return imgMhs;
    }

    public void setImgMhs(String imgMhs) {
        this.imgMhs = imgMhs;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNim_progmob() {
        return nim_progmob;
    }

    public void setNim_progmob(String nim_progmob) {
        this.nim_progmob = nim_progmob;
    }
}
