package ukdw.com.helloworld.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.helloworld.CrudMatkul.MatkulUpdateActivity;
import ukdw.com.helloworld.Model.Matkul;
import ukdw.com.helloworld.R;

public class MatkulCRUDAdapter extends RecyclerView.Adapter<MatkulCRUDAdapter.ViewHolder>
{
    private Context context;
    private List<Matkul> matkulList = new ArrayList<>();

    public List<Matkul> getDosenList(){return matkulList;}

    public void setMatkulList(List<Matkul> matkulList) {
        this.matkulList = matkulList;
        //jika ada perubahan maka akan memberitahu android sehingga android merubah secara otomatis
        notifyDataSetChanged();
    }

    public MatkulCRUDAdapter(Context context) {
        this.context = context;
        matkulList = new ArrayList<>();
    }
    public MatkulCRUDAdapter(List<Matkul> matkulList) {
        this.matkulList = matkulList;
    }

    @NonNull
    @Override
    public MatkulCRUDAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_matkul,parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //binding disini (sambungkan datanya disini)
        Matkul m = matkulList.get(position);
        holder.tvNama.setText(m.getNama());
        holder.tvKode.setText(m.getKode());
        /*holder.tvHari.setText(m.getHari());
        holder.tvSesi.setText(m.getSesi());
        holder.tvSks.setText(m.getSesi());*/
        /*holder.tvHari.setText(String.valueOf(m.getHari()));
        holder.tvSesi.setText(String.valueOf(m.getSesi()));
        holder.tvSks.setText(String.valueOf(m.getSks()));*/
        holder.tvHari.setText(m.getHari().toString());
        holder.tvSesi.setText(m.getSesi().toString());
        holder.tvSks.setText(m.getSesi().toString());
        holder.m = m;
    }

    @Override
    public int getItemCount() {
        return matkulList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama, tvKode, tvHari, tvSesi, tvSks;
        Matkul m;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvKode = itemView.findViewById(R.id.tvKode);
            tvHari = itemView.findViewById(R.id.tvHari);
            tvSesi = itemView.findViewById(R.id.tvSesi);
            tvSks = itemView.findViewById(R.id.tvSks);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentUpdate = new Intent(itemView.getContext(), MatkulUpdateActivity.class);
                    intentUpdate.putExtra("nama", m.getNama());
                    intentUpdate.putExtra("kode", m.getKode());
                    intentUpdate.putExtra("hari", m.getHari().toString());
                    intentUpdate.putExtra("sesi", m.getSesi().toString());
                    intentUpdate.putExtra("sks", m.getSks().toString());
                    Bundle b = new Bundle();
                    intentUpdate.putExtras(b);
                    itemView.getContext().startActivity(intentUpdate);
                }
            });
        }
    }
}
