package ukdw.com.helloworld.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.helloworld.Model.MahasiswaDebugging;
import ukdw.com.helloworld.R;

public class DebuggingRecyclerAdapter extends RecyclerView.Adapter<DebuggingRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<MahasiswaDebugging> mahasiswaDebuggingList;

    public List<MahasiswaDebugging> getMahasiswaDebuggingList() {
        return mahasiswaDebuggingList;
    }

    public void setMahasiswaDebuggingList(List<MahasiswaDebugging> mahasiswaDebuggingList) {
        this.mahasiswaDebuggingList = mahasiswaDebuggingList;
        notifyDataSetChanged();
    }

    public DebuggingRecyclerAdapter(Context context) {
        this.context = context;
        mahasiswaDebuggingList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_list_debugging,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MahasiswaDebugging m = mahasiswaDebuggingList.get(position);
        holder.tvNama.setText(m.getNama());
        holder.tvNim.setText(m.getNim());
        holder.tvNoTelp.setText(m.getNotelp());

    }

    @Override
    public int getItemCount() {
        return mahasiswaDebuggingList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama,tvNim,tvNoTelp;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvNim = itemView.findViewById(R.id.tvNim);
            tvNoTelp = itemView.findViewById(R.id.tvNoTelp);
        }
    }
}
