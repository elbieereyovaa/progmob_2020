package ukdw.com.helloworld.Adapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.helloworld.Crud.MahasiswaUpdateActivity;
import ukdw.com.helloworld.Model.Mahasiswa;
import ukdw.com.helloworld.R;

public class MahasiswaCRUDRecyclerAdapter extends RecyclerView.Adapter<MahasiswaCRUDRecyclerAdapter.ViewHolder>{
    private Context context;
    private List<Mahasiswa> mahasiswaList;

    public List<Mahasiswa> getMahasiswaList() {
        return mahasiswaList;
    }

    public void setMahasiswaList(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
        //jika ada perubahan maka akan memberitahu android sehingga android merubah secara otomatis
        notifyDataSetChanged();
    }

    public MahasiswaCRUDRecyclerAdapter(Context context) {
        this.context = context;
        mahasiswaList = new ArrayList<>();
    }

    public MahasiswaCRUDRecyclerAdapter(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview,parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //binding disini (sambungkan datanya disini)
        Mahasiswa m = mahasiswaList.get(position);
        holder.tvNama.setText(m.getNama());
        holder.tvNim.setText(m.getNim());
        holder.tvAlamat.setText(m.getAlamat());
        holder.tvEmail.setText(m.getEmail());
        //holder.tvNoTelp.setText(m.getNoTelp());
        holder.m = m;
    }


    @Override
    public int getItemCount() {
        return mahasiswaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvNama, tvNim, tvNoTelp,tvEmail, tvAlamat;
        Mahasiswa m;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvNim = itemView.findViewById(R.id.tvNim);
            tvAlamat = itemView.findViewById(R.id.tvAlamat);
            tvEmail = itemView.findViewById(R.id.tvEmail);

            //tvNoTelp = itemView.findViewById(R.id.txtNoTelp);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentUpdate = new Intent(itemView.getContext(), MahasiswaUpdateActivity.class);
                    intentUpdate.putExtra("nama", m.getNama());
                    intentUpdate.putExtra("nim", m.getNim());
                    intentUpdate.putExtra("alamat", m.getAlamat());
                    intentUpdate.putExtra("email", m.getEmail());
                    Bundle b = new Bundle();
                    intentUpdate.putExtras(b);
                    itemView.getContext().startActivity(intentUpdate);
                }
            });
        }
    }
}
