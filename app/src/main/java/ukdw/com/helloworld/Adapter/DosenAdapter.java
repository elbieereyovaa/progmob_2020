package ukdw.com.helloworld.Adapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.helloworld.crudDosen.DosenUpdateActivity;
import ukdw.com.helloworld.Model.Dosen;
import ukdw.com.helloworld.R;

public class DosenAdapter extends RecyclerView.Adapter<DosenAdapter.ViewHolder> {
    private Context context;
    private List<Dosen> dosenList;

    public List<Dosen> getDosenList(){return dosenList;}

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
        //jika ada perubahan maka akan memberitahu android sehingga android merubah secara otomatis
        notifyDataSetChanged();
    }

    public DosenAdapter(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();
    }
    public DosenAdapter(List<Dosen> dosenList) {
        this.dosenList = dosenList;
    }

    @NonNull
    @Override
    public DosenAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_dosen,parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //binding disini (sambungkan datanya disini)
        Dosen d = dosenList.get(position);
        holder.tvNama.setText(d.getNama());
        holder.tvNidn.setText(d.getNidn());
        holder.tvAlamat.setText(d.getAlamat());
        holder.tvEmail.setText(d.getEmail());
        holder.tvGelar.setText(d.getGelar());
        //holder.tvNoTelp.setText(d.getNoTelp());
        holder.d = d;
    }


    @Override
    public int getItemCount() {
        return dosenList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama, tvNidn, tvAlamat, tvEmail, tvGelar;
        Dosen d;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvNidn = itemView.findViewById(R.id.tvNidn);
            tvAlamat= itemView.findViewById(R.id.tvAlamat);
            tvEmail = itemView.findViewById(R.id.tvEmail);
            tvGelar = itemView.findViewById(R.id.tvGelar);

            //tvNoTelp = itemView.findViewById(R.id.txtNoTelp);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentUpdate = new Intent(itemView.getContext(), DosenUpdateActivity.class);
                    intentUpdate.putExtra("nama", d.getNama());
                    intentUpdate.putExtra("nidn", d.getNidn());
                    intentUpdate.putExtra("alamat", d.getAlamat());
                    intentUpdate.putExtra("email", d.getEmail());
                    intentUpdate.putExtra("gelar", d.getGelar());
                    Bundle b = new Bundle();
                    intentUpdate.putExtras(b);
                    itemView.getContext().startActivity(intentUpdate);
                }
            });
        }
    }
}
