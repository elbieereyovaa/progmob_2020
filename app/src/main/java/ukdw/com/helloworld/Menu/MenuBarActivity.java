package ukdw.com.helloworld.Menu;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.solver.state.State;
import androidx.constraintlayout.widget.ConstraintLayout;
//import android.support.design.widget.Snackbar;

import com.google.android.material.snackbar.Snackbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;


import ukdw.com.helloworld.Crud.MahasiswaGetAllActivity;
import ukdw.com.helloworld.Crud.MainMhsActivity;
import ukdw.com.helloworld.CrudMatkul.MatkulGetAllActivity;
import ukdw.com.helloworld.Pertemuan6.PrefActivity;
import ukdw.com.helloworld.R;
import ukdw.com.helloworld.Splashscreen.LoginActivity;
import ukdw.com.helloworld.crudDosen.DosenGetAllActivity;


public class MenuBarActivity extends AppCompatActivity {
    LinearLayout currentLayout;
    Snackbar mySnackbar;
    String isLogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_bar);
        Toolbar myToolBar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(myToolBar);
        //ganti title bar
        getSupportActionBar().setTitle("Hai");

        //button2
        ImageView showMhs = (ImageView)findViewById(R.id.menuMhs);
        ImageView showDsn = (ImageView)findViewById(R.id.menuDosen);
        ImageView showMatkul = (ImageView)findViewById(R.id.menuMatkul);

        showMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuBarActivity.this, MahasiswaGetAllActivity.class);
                startActivity(intent);
            }
        });
        showDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuBarActivity.this, DosenGetAllActivity.class);
                startActivity(intent);
            }
        });
        showMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuBarActivity.this, MatkulGetAllActivity.class);
                startActivity(intent);
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item){
       switch(item.getItemId()){
           case R.id.logout:
               showDialog();
               //logout();
               return true;

           default:
               return super.onOptionsItemSelected(item);
       }
    }
    private void logout() {
        SharedPreferences pref = MenuBarActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        pref.edit().clear().commit();
        SharedPreferences.Editor editor = pref.edit();
        isLogin = pref.getString("isLogin","0");
        editor.putString("isLogin","0");
        editor.commit();
        Intent intentLogout = new Intent(MenuBarActivity.this, LoginActivity.class);
        intentLogout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentLogout);
    }

    private void showDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(
                MenuBarActivity.this
        );
        builder.setTitle("Keluar dari aplikasi?");
        builder.setMessage("Apakah Anda Ingin Logout?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                       logout();
                //MenuBarActivity.this.finish();
            }
        });


        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}