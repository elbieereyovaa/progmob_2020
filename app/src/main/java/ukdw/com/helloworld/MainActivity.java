package ukdw.com.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import ukdw.com.helloworld.Pertemuan2.CardViewActivity;
import ukdw.com.helloworld.Pertemuan2.ListActivity;
import ukdw.com.helloworld.Pertemuan2.RecyclerActivity;
import ukdw.com.helloworld.Pertemuan4.DebuggingActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    //kalau ada tulisan oncreate, code utama dari android
    //jadi akan menjalankan function dan program di oncreate
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //mendapatkan textview yang ada di layout
        //menghubungkan kelas java dengan layout
        //variable
        final TextView txtView = (TextView)findViewById(R.id.mainActivityTextView);
        Button myBtn = (Button)findViewById(R.id.button1);
        final EditText myEditText = (EditText)findViewById(R.id.editText1);
        Button btnHelp = (Button)findViewById(R.id.btnHelp);
        Button btnTugas = (Button)findViewById(R.id.btnTugas);

        //pertemuan2
        Button btnList = (Button)findViewById(R.id.btnList);
        Button btnRecycler = (Button)findViewById(R.id.btnRecycler);
        Button btnCard = (Button)findViewById(R.id.btnCard);

        //pertemuan4
        Button btnPertemuan = (Button)findViewById(R.id.btnPertemuan);

        //action
        txtView.setText(R.string.text_hello_world);
        myBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //log akan muncul di logCat
                //Log.d("COBA KLIKKKKKK", myEditText.getText().toString());

                //ganti di text view yang di atas (tengah)
                txtView.setText(myEditText.getText().toString());
            }
        });
        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pindah activity pake intent
                Intent intent = new Intent(MainActivity.this, HelpActivity.class);
                Bundle b = new Bundle();

                b.putString("help_string", myEditText.getText().toString());
                //simpan
                intent.putExtras(b);
                startActivity(intent);
            }
        });
        btnTugas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,TugasActivity2.class);
                startActivity(intent);
            }
        });

        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                startActivity(intent);
            }
        });

        btnRecycler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RecyclerActivity.class);
                startActivity(intent);
            }
        });

        btnCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                startActivity(intent);
            }
        });

        btnPertemuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DebuggingActivity.class);
                startActivity(intent);
            }
        });







    }
}