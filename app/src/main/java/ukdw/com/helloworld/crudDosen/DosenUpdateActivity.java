package ukdw.com.helloworld.crudDosen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.helloworld.Crud.MahasiswaGetAllActivity;
import ukdw.com.helloworld.Crud.MahasiswaUpdateActivity;
import ukdw.com.helloworld.Model.DefaultResult;
import ukdw.com.helloworld.Network.GetDataService;
import ukdw.com.helloworld.Network.RetrofitClientInstance;
import ukdw.com.helloworld.R;

public class DosenUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;
    Snackbar mySnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_update);
        Toolbar myToolBar = (Toolbar)findViewById(R.id.toolbarUpdateDsn);
        setSupportActionBar(myToolBar);
        //ganti title bar
        getSupportActionBar().setTitle("Update Data Dosen");

        EditText edNidnCari = (EditText) findViewById(R.id.editTextNidnCari);
        EditText edNamaBaru = (EditText)findViewById(R.id.editTextNama2);
        EditText edNidnBaru = (EditText)findViewById(R.id.editTextNidn2);
        EditText edAlamatBaru = (EditText)findViewById(R.id.editTextAlamatDsn2);
        EditText edEmailBaru = (EditText)findViewById(R.id.editTextEmailDsn2);
        EditText edGelarBaru = (EditText)findViewById(R.id.editTextGelarDsn2);
        Button btnUpdateDsn = (Button)findViewById(R.id.btnUbahDsn);
        pd = new ProgressDialog(DosenUpdateActivity.this);

        Intent data = getIntent();
        if(data.getExtras() != null) {
            edNidnCari.setText(data.getStringExtra("nidn"));
            edNamaBaru.setText(data.getStringExtra("nama"));
            edNidnBaru.setText(data.getStringExtra("nidn"));
            edAlamatBaru.setText(data.getStringExtra("alamat"));
            edEmailBaru.setText(data.getStringExtra("email"));
            edGelarBaru.setText(data.getStringExtra("gelar"));
        }

        btnUpdateDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.setCancelable(false);
                pd.show();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                //memanggil service yang nantinya mengembalikan list mahasiswa
                Call<DefaultResult> call = service.update_dosen(
                        edNamaBaru.getText().toString(),
                        edNidnBaru.getText().toString(),
                        edAlamatBaru.getText().toString(),
                        edEmailBaru.getText().toString(),
                        edGelarBaru.getText().toString(),
                        "kosongkan saja",
                        "72180196",
                        edNidnCari.getText().toString()
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this, "DATA BERHASIL DIUBAH", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
                    }
                });
                Intent intentUpdate = new Intent(DosenUpdateActivity.this, DosenGetAllActivity.class);
                startActivity(intentUpdate);
                finish();
            }
        });
    }
}