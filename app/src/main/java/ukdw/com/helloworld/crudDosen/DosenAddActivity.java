package ukdw.com.helloworld.crudDosen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.helloworld.Crud.MahasiswaAddActivity;
import ukdw.com.helloworld.Crud.MahasiswaGetAllActivity;
import ukdw.com.helloworld.Model.DefaultResult;
import ukdw.com.helloworld.Network.GetDataService;
import ukdw.com.helloworld.Network.RetrofitClientInstance;
import ukdw.com.helloworld.R;

public class DosenAddActivity extends AppCompatActivity {
    ProgressDialog pd;
    LinearLayout currentLayout;
    Snackbar mySnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_add);
        EditText edNama = (EditText)findViewById(R.id.editTextNama);
        EditText edNidn = (EditText)findViewById(R.id.editTextNidn);
        EditText edAlamat = (EditText)findViewById(R.id.editTextAlamat);
        EditText edEmail = (EditText)findViewById(R.id.editTextEmail);
        EditText edGelar = (EditText)findViewById(R.id.editTextGelarDsn);
        Button btnSimpan = (Button)findViewById(R.id.btnSimpanDsn);
        Spinner spinnerHari = (Spinner)findViewById(R.id.spinnerHari);

        pd = new ProgressDialog(DosenAddActivity.this);
        Toolbar myToolBar = (Toolbar)findViewById(R.id.toolbarAddDsn);
        setSupportActionBar(myToolBar);
        //ganti title bar
        getSupportActionBar().setTitle("Tambah Data Dosen");


        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //membuat progress dialog
                //pd.setTitle("Mohon Menunggu");
                //pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                //memanggil service yang nantinya mengembalikan list mahasiswa
                Call<DefaultResult> call = service.add_dosen(
                        edNama.getText().toString(),
                        edNidn.getText().toString(),
                        edAlamat.getText().toString(),
                        edEmail.getText().toString(),
                        edGelar.getText().toString(),
                        "kosongkan saja diisi sembarang karena dirandom sistem",
                        "72180196"

                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DosenAddActivity.this,"DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenAddActivity.this,"GAGAL", Toast.LENGTH_LONG).show();
                    }
                });
                Intent intent = new Intent(DosenAddActivity.this, DosenGetAllActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}