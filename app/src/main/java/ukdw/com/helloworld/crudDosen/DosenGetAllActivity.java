package ukdw.com.helloworld.crudDosen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.helloworld.Adapter.DosenAdapter;
import ukdw.com.helloworld.Model.Dosen;
import ukdw.com.helloworld.Network.GetDataService;
import ukdw.com.helloworld.Network.RetrofitClientInstance;
import ukdw.com.helloworld.R;

public class DosenGetAllActivity extends AppCompatActivity {
    RecyclerView rvDsn;
    DosenAdapter dsnAdapter;
    ProgressDialog pd;
    List<Dosen> dosenList; //data akan diisi ke internet
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_get_all);
        rvDsn = (RecyclerView) findViewById(R.id.rvDosen);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Bersabar"); //text akan muncul ketika melakukan pemanggilan data
        pd.show(); //lingkaran loading
        Toolbar myToolBar = (Toolbar)findViewById(R.id.toolbarDsnAll);
        setSupportActionBar(myToolBar);
        //ganti title bar
        getSupportActionBar().setTitle("Data Dosen");

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        //memanggil service yang nantinya mengembalikan list mahasiswa
        Call<List<Dosen>> call = service.getDosen("72180196");

        //dengan menggunakan objek call, akan dilakukan enqueue (diurutkan)
        call.enqueue(new Callback<List<Dosen>>() {
            @Override
            //fungsi yang berjalan ketika berhasil mengolah data
            public void onResponse(Call<List<Dosen>> call, Response<List<Dosen>> response) {
                pd.dismiss(); //menghilangkan progress dialog (lingkaran loading)
                dosenList = response.body(); //mahasiswa list diisi dari hasil respon
                dsnAdapter = new DosenAdapter(dosenList); //lalu diisi ke adapter

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DosenGetAllActivity.this);
                rvDsn.setLayoutManager(layoutManager);
                rvDsn.setAdapter(dsnAdapter);

            }

            @Override
            //onfailure akan terjadi jika gagal dalam mengolah data
            public void onFailure(Call<List<Dosen>> call, Throwable t) {
                pd.dismiss();
                //dikasih alert kalo terjadi error
                Toast.makeText(DosenGetAllActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenudosentambah, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.tambahDsn:
                Intent intentAdd = new Intent(this, DosenAddActivity.class);
                startActivity(intentAdd);
                return true;
            case (R.id.hapusDsn):
                Intent intentHps = new Intent(this, HapusDosenActivity.class);
                startActivity(intentHps);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}