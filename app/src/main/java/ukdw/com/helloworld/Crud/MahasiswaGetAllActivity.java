package ukdw.com.helloworld.Crud;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.helloworld.Adapter.MahasiswaCRUDRecyclerAdapter;
import ukdw.com.helloworld.Menu.MenuBarActivity;
import ukdw.com.helloworld.Model.Mahasiswa;
import ukdw.com.helloworld.Network.GetDataService;
import ukdw.com.helloworld.Network.RetrofitClientInstance;
import ukdw.com.helloworld.Pertemuan6.PrefActivity;
import ukdw.com.helloworld.R;

public class MahasiswaGetAllActivity extends AppCompatActivity {

    RecyclerView rvMhs;
    MahasiswaCRUDRecyclerAdapter mhsAdapter;
    ProgressDialog pd;
    List<Mahasiswa> mahasiswaList; //data akan diisi ke internet
    LinearLayout currentLayout;
    Snackbar mySnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_get_all);

        rvMhs = (RecyclerView) findViewById(R.id.rvGetMhsAll);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Bersabar"); //text akan muncul ketika melakukan pemanggilan data
        pd.show(); //lingkaran loading
        Toolbar myToolBar = (Toolbar)findViewById(R.id.toolbarMhs);
        setSupportActionBar(myToolBar);
        //ganti title bar
        getSupportActionBar().setTitle("Data Mahasiswa");


        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        //memanggil service yang nantinya mengembalikan list mahasiswa
        Call<List<Mahasiswa>> call = service.getMahasiswa("72180196");

        //dengan menggunakan objek call, akan dilakukan enqueue (diurutkan)
        call.enqueue(new Callback<List<Mahasiswa>>() {
            @Override
            //fungsi yang berjalan ketika berhasil mengolah data
            public void onResponse(Call<List<Mahasiswa>> call, Response<List<Mahasiswa>> response) {
                pd.dismiss(); //menghilangkan progress dialog (lingkaran loading)
                mahasiswaList = response.body(); //mahasiswa list diisi dari hasil respon
                mhsAdapter = new MahasiswaCRUDRecyclerAdapter(mahasiswaList); //lalu diisi ke adapter

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MahasiswaGetAllActivity.this);
                rvMhs.setLayoutManager(layoutManager);
                rvMhs.setAdapter(mhsAdapter);
            }
            @Override
            //onfailure akan terjadi jika gagal dalam mengolah data
            public void onFailure(Call<List<Mahasiswa>> call, Throwable t) {
                pd.dismiss();
                //dikasih alert kalo terjadi error
                Toast.makeText(MahasiswaGetAllActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optiontambah, menu);
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.tambahMhs:
                Intent intent = new Intent(this, MahasiswaAddActivity.class);
                startActivity(intent);
                return true;
            case R.id.hapusMhs:
                Intent intentHps = new Intent(this, HapusMhsActivity.class);
                startActivity(intentHps);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    }
