package ukdw.com.helloworld.Crud;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.os.IResultReceiver;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.helloworld.Adapter.MahasiswaCRUDRecyclerAdapter;
import ukdw.com.helloworld.Model.DefaultResult;
import ukdw.com.helloworld.Model.Mahasiswa;
import ukdw.com.helloworld.Network.GetDataService;
import ukdw.com.helloworld.Network.RetrofitClientInstance;
import ukdw.com.helloworld.R;

public class MahasiswaUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;
    Snackbar mySnackbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);

        Toolbar myToolBar = (Toolbar)findViewById(R.id.toolbar_update);
        setSupportActionBar(myToolBar);
        //ganti title bar
        getSupportActionBar().setTitle("Update Data Mahasiswa");

        EditText edNimCari = (EditText) findViewById(R.id.editTextNimCari);
        EditText edNamaBaru = (EditText)findViewById(R.id.editTextNama2);
        EditText edNimBaru = (EditText)findViewById(R.id.editTextNim2);
        EditText edAlamatBaru = (EditText)findViewById(R.id.editTextAlamat2);
        EditText edEmailBaru = (EditText)findViewById(R.id.editTextEmail2);
        Button btnUpdateMhs = (Button)findViewById(R.id.btnUbahMhs);
        pd = new ProgressDialog(MahasiswaUpdateActivity.this);

        Intent data = getIntent();
        if(data.getExtras() != null) {
            edNimCari.setText(data.getStringExtra("nim"));
            edNamaBaru.setText(data.getStringExtra("nama"));
            edNimBaru.setText(data.getStringExtra("nim"));
            edAlamatBaru.setText(data.getStringExtra("alamat"));
            edEmailBaru.setText(data.getStringExtra("email"));
        }

            btnUpdateMhs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //membuat progress dialog
                    pd.setTitle("Mohon Menunggu");
                    pd.setCancelable(false);
                    pd.show();

                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    //memanggil service yang nantinya mengembalikan list mahasiswa
                    Call<DefaultResult> call = service.update_mhs(
                            edNamaBaru.getText().toString(),
                            edNimBaru.getText().toString(),
                            edNimCari.getText().toString(),
                            edAlamatBaru.getText().toString(),
                            edEmailBaru.getText().toString(),
                            "kosongkan saja diisi sembarang karena dirandom sistem",
                            "72180196"
                    );
                    call.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            pd.dismiss();
                            Toast.makeText(MahasiswaUpdateActivity.this, "DATA BERHASIL DIUBAH", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(MahasiswaUpdateActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
                        }
                    });
                    Intent intentUpdate = new Intent(MahasiswaUpdateActivity.this, MahasiswaGetAllActivity.class);
                    startActivity(intentUpdate);
                    finish();
                }
            });

    }
}