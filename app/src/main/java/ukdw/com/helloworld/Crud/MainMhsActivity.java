package ukdw.com.helloworld.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.helloworld.Adapter.MahasiswaCRUDRecyclerAdapter;
import ukdw.com.helloworld.Model.Mahasiswa;
import ukdw.com.helloworld.R;


public class MainMhsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mhs);


        Button btnLihatMhs = (Button)findViewById(R.id.btnGetMhs);
        Button btnTambahMhs = (Button)findViewById(R.id.btnAddMhs);
        Button btnEditMhs = (Button)findViewById(R.id.btnUpdateMhs);
        Button btnHapusMhs = (Button)findViewById(R.id.btnDeleteMhs);

        btnLihatMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this, MahasiswaGetAllActivity.class);
                startActivity(intent);
            }
        });

        btnTambahMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this, MahasiswaAddActivity.class);
                startActivity(intent);
            }
        });

        btnEditMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this,MahasiswaGetAllActivity.class);
                startActivity(intent);
            }
        });

        btnHapusMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this,HapusMhsActivity.class);
                startActivity(intent);
            }
        });
    }

}