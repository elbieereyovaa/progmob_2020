package ukdw.com.helloworld.Splashscreen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.helloworld.Menu.MenuBarActivity;
import ukdw.com.helloworld.Model.UserData;
import ukdw.com.helloworld.Network.GetDataService;
import ukdw.com.helloworld.Network.RetrofitClientInstance;
import ukdw.com.helloworld.R;
import ukdw.com.helloworld.Storage.SharedPrefManager;

public class LoginActivity extends AppCompatActivity {
    private final String TAG = "Login Activity";
    SharedPrefManager sharedPrefManager;
    String isLogin = "";


    private EditText edLogin, edPass;
    private Button btnLogin;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edLogin = (EditText) findViewById(R.id.edNimLogin);
        edPass = (EditText) findViewById(R.id.edPasswordLogin);
        btnLogin = (Button) findViewById(R.id.btnPref);
        pd = new ProgressDialog(LoginActivity.this);
        SharedPreferences pref  = LoginActivity.this.getSharedPreferences("prefs_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        isLogin = pref.getString("isLogin", "0");
        if(isLogin.equals("1")){
            Intent intent = new Intent(LoginActivity.this, MenuBarActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            //finish();
        }


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogin();
                editor.putString("isLogin","1");
                editor.commit();
            }
        });
    }

    private void userLogin() {
        String nimnik = edLogin.getText().toString();
        String password = edPass.getText().toString();

        //validasi NIM
          if(nimnik.isEmpty()){
            edLogin.setError("NIM is Required!");
            edLogin.requestFocus();
            return;
        }
        if(nimnik.length() < 8){
            edLogin.setError("NIM Should be 8 Character!");
            edLogin.requestFocus();
            return;
        }else if(nimnik.length() > 8){
            edLogin.setError("NIM Should be 8 Character!");
            edLogin.requestFocus();
            return;
        }
        //validation password
        if(password.isEmpty()){
            edPass.setError("Password is Required!");
            edPass.requestFocus();
            return;
        }
        pd.setTitle("Logging in");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<UserData[]> call = service.login_admin_array(
                edLogin.getText().toString(),
                edPass.getText().toString()
        );
        call.enqueue(new Callback<UserData[]>() {
            @Override
            public void onResponse(Call<UserData[]> call, Response<UserData[]> response) {
                pd.dismiss();
                UserData[] users = response.body();
                if (users.length > 0) {
                    //error
                    /*if (users[0] != null) {
                        Log.d(TAG, "loginResponse[0] found " + users[0].getNama());
                } else {
                    Toast.makeText(LoginActivity.this, "Login Error" + response.message(), Toast.LENGTH_LONG).show();
                    //proceed with the login karena users masih blm ada isinya brti msh blm login
                    //save user to shared preferences
                    }*/
                    SharedPrefManager.getInstance(LoginActivity.this).saveUser(users[0]);
                    //open new activity
                    Intent intent = new Intent(LoginActivity.this, MenuBarActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    //finish();
                }
            }

            @Override
            public void onFailure(Call<UserData[]> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(LoginActivity.this, "Login Failed!", Toast.LENGTH_LONG).show();
                Log.e(TAG, t.getMessage());
            }
        });
    }

    //yang selalu dijalankan pertama
    @Override
    protected void onStart() {
        super.onStart();
        //kalau user sudah login
        /*if(SharedPrefManager.getInstance(this).isLoggedIn()){
            Intent intent = new Intent(LoginActivity.this, MenuBarActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }*/
    }


 }
