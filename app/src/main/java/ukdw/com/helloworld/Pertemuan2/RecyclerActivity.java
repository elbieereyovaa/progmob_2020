package ukdw.com.helloworld.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.helloworld.Adapter.MahasiswaRecyclerAdapter;
import ukdw.com.helloworld.Model.Mahasiswa;
import ukdw.com.helloworld.R;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvlatian);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        //data dummy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("Elbie", "72180196", "0829876543");
        Mahasiswa m2 = new Mahasiswa("Nadia", "72180200", "0829876543");
        Mahasiswa m3 = new Mahasiswa("Evi", "72180180", "081345678992");
        Mahasiswa m4 = new Mahasiswa("Felix", "72180190", "0821234567");
        Mahasiswa m5 = new Mahasiswa("Pyo", "72180001", "081122345654");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(RecyclerActivity.this);
        //memasukkan list disini ke adapter
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);

    }

}