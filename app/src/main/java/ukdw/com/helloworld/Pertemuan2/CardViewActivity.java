package ukdw.com.helloworld.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.helloworld.Adapter.DosenAdapter;
import ukdw.com.helloworld.Adapter.MahasiswaRecyclerAdapter;
import ukdw.com.helloworld.Model.Dosen;
import ukdw.com.helloworld.Model.Mahasiswa;
import ukdw.com.helloworld.R;

public class CardViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvActivity);
        DosenAdapter dosenAdapter;

        //data dummy
        List<Dosen> dosenList = new ArrayList<Dosen>();

        //generate data Dosen
        Dosen d1 = new Dosen("Elbie", "72180196", "0829876543");
        Dosen d2 = new Dosen("Nadia", "72180200", "0829876543");
        Dosen d3 = new Dosen("Evi", "72180180", "081345678992");
        Dosen d4 = new Dosen("Felix", "72180190", "0821234567");
        Dosen d5 = new Dosen("Pyo", "72180001", "081122345654");

        dosenList.add(d1);
        dosenList.add(d2);
        dosenList.add(d3);
        dosenList.add(d4);
        dosenList.add(d5);

        dosenAdapter = new DosenAdapter(CardViewActivity.this);
        //memasukkan list disini ke adapter
        dosenAdapter.setDosenList(dosenList);

        rv.setLayoutManager(new LinearLayoutManager(CardViewActivity.this));
        rv.setAdapter(dosenAdapter);

    }
}