package ukdw.com.helloworld.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//import ukdw.com.helloworld.MainHome.MainHomeActivity;
import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.helloworld.Menu.MenuBarActivity;
import ukdw.com.helloworld.Model.DefaultResult;
import ukdw.com.helloworld.Model.Dosen;
import ukdw.com.helloworld.Model.UserData;
import ukdw.com.helloworld.Network.GetDataService;
import ukdw.com.helloworld.Network.RetrofitClientInstance;
import ukdw.com.helloworld.R;

public class PrefActivity extends AppCompatActivity {
    String isLogin;
    ProgressDialog pd;
    //Context context;
    EditText edLogin, edPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);

        edLogin = (EditText) findViewById(R.id.edNimLogin);
        edPass = (EditText) findViewById(R.id.edPasswordLogin);
        Button btnPref = (Button) findViewById(R.id.btnPref);
        pd = new ProgressDialog(PrefActivity.this);

        SharedPreferences pref = PrefActivity.this.getSharedPreferences("prefs_file", MODE_PRIVATE);
        //kalo ngisi preferences pakai ini
        //editor itu tools dari sharedpreferences untuk edit value (yang diedit pref)
        SharedPreferences.Editor editor = pref.edit();
        //pilih mode private biar tidak semua aplikasi bisa mengakses

        //membaca preferences login apakah true atau false
        /*isLogin = pref.getString("isLogin", "0");
        if (isLogin.equals("1")) {
            //jika berhasil buttonnya jadi logout
            btnPref.setText("Logout");
        } else {
            btnPref.setText("Login");
        }*/
        //pengisian preferences ketika tombol di klik
        btnPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<List<UserData>> call = service.login(
                        edLogin.getText().toString(),
                        edPass.getText().toString()
                );
                if (TextUtils.isEmpty(edLogin.getText().toString()) ||
                        TextUtils.isEmpty(edPass.getText().toString())) {
                    String message = "All Input Required...";
                    Toast.makeText(PrefActivity.this, message, Toast.LENGTH_LONG).show();
                } else {
                    call.enqueue(new Callback<List<UserData>>() {
                        @Override
                        public void onResponse(Call<List<UserData>> call, Response<List<UserData>> response) {
                            pd.dismiss();
                            Toast.makeText(PrefActivity.this, "Login Berhasil", Toast.LENGTH_LONG).show();
                            Intent intentAddData = new Intent(PrefActivity.this, MenuBarActivity.class);
                            startActivity(intentAddData);
                        }

                        @Override
                        public void onFailure(Call<List<UserData>> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(PrefActivity.this, "Login Gagal! Silahkan Cek Username / Password Kembali!", Toast.LENGTH_LONG).show();
                        }*/


                isLogin = pref.getString("isLogin","0");
                if(isLogin.equals("0")){
                    //kalo belom login
                    editor.putString("isLogin", "1");
                    btnPref.setText("Logout");

                    Intent intent = new Intent(PrefActivity.this, MenuBarActivity.class);
                    startActivity(intent);
                } else{
                    editor.putString("isLogin", "0");
                    btnPref.setText("Login");
                }

                //menyimpan perubahan di preferences
                editor.commit();


            }
        });
    }
}

